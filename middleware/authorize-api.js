const expressJwt = require('express-jwt');
const { secret } = require('../config/app-config.json').jwt;
const db = require('../models/index');

module.exports = authorize;

//Check validity of JWT and permissions on user
function authorize(permissions){
    if(typeof permissions === 'string')
        permissions = [permissions]

    return [
        expressJwt({secret}),

        (req, res, next) => {
            db.users.findAll({
                where:{
                    id: req.user.sub
                },
                include:{
                    model: db.permissions
                }
            }).then((user) => {
                if(user[0]){
                    var permissionsArray = Array.from(user[0].permissions, x => x.name);
                    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

                    /*if(req.user.ip != ip){
                        return res.status(401).json({
                            error: true,
                            message: 'Invalid token'
                        });
                    }*/

                    if(permissionsArray.includes('admin')){
                        next();
                    }else if(permissions.length && !permissions.every(x => permissionsArray.includes(x))){
                        return res.status(401).json({
                            error: true,
                            message: 'Unauthorized'
                        });
                    }else{
                        next();
                    }
                }else{
                    return res.status(401).json({
                        error: true,
                        message: 'Unauthorized'
                    });
                }
            });
        }
    ]
}