var userController = require('../controllers/users');

module.exports = checkLogin;

function checkLogin(req, res, next){
    if(req.path != '/login'){
        if(!req.session.token){
            return res.redirect('/login');
        }else{
            userController.getFromId(req.session.userId, (user) => {
                req.user = user;
                next(); 
            });
        }
    }else{
        if(req.session.token){
            userController.getFromId(req.session.userId, (user) => {
                req.user = user;
                return res.redirect('/');
            });
        }else{
            next();
        }
    }
}