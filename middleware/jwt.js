const expressJwt = require('express-jwt');
const config = require('../config/app-config.json');

module.exports = jwt;

//Apply restriction on all routes except /auth/login
function jwt() {
    const secret = config.jwt.secret;
    return expressJwt({ secret }).unless({
        path: [
            '/auth/login'
        ]
    });
}