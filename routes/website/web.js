var express = require('express');
var db = require('../../models/index');
var loginController = require('../../controllers/login');
var userController = require('../../controllers/users');
var authorizeAPI = require('../../middleware/authorize-api');
const request = require('request');

module.exports = function(app){
    var router = express.Router();

    router.get('/', function(req, res){
        console.log(req.session); 

        db.devices_data.findAll({
            where:{}
        }).then((data) => {
            data.forEach(d => d.data = JSON.parse(d.data));
            takeData = data.filter(d => d.data);
            takeData = takeData.filter(d => d.data.action == 'take');
            takeData = takeData.filter(d => new Date(d.createdAt).getDate() == new Date().getDate());

            var qSold = 0;

            takeData.forEach(d => qSold += d.data.quantity);


            db.devices.count({
                where:{}
            }).then((count) => {
                var soldData = [];

                takeData.forEach((d) => {
                    let date = new Date(d.createdAt)
                    soldData.push({
                        x: date,
                        y: d.data.quantity
                    })
                });

                res.render('home', {title: 'Dashboard', user: req.user, sold: qSold, nbDevices: count, soldData: soldData, token: req.session.token, ip: req.hostname});
            });
        });
    });

    router.get('/login', function(req, res){
        if(req.session.token)
            return res.redirect('/');

        var err = req.query.err;
        var username = req.query.username;

        res.render('login', {title: 'IoT Manager - Login', error: err, username: username});
    });

    router.post('/login', loginController.login);

    router.get('/logout', (req, res) => {
        req.session.destroy((err) => {
            if(err){
                console.log(err);
            }

            return res.redirect('/login');
        });
    });

    router.get('/devices', function(req, res){

        request.get('http://localhost:3000/api/devices', {
            headers:{
                Authorization: 'Bearer ' + req.session.token,
                Accept: 'application/json'
            }
        },
        (error, response, body) => {
            body = JSON.parse(body);
            if(error || response.statusCode != 200){
                console.error(error);

                let err;

                if(response.statusCode == 401 || response.statusCode == 403)
                    err = 'Vous n\'avez pas les droits suffisants';
                else    
                    err = 'Une erreur est survenue';

                return res.render('devices', {title: 'Devices', user: req.user, devices: null, error: err, token: req.session.token, ip: req.hostname});
            }else{
                if(body.error){
                    console.error(body.errorCode + ': ' + body.message);
                    return res.render('devices', {title: 'Devices', user: req.user, devices: null, error: 'Une erreur est survenue', token: req.session.token, ip: req.hostname});
                }else{
                    return res.render('devices', {title: 'Devices', user: req.user, devices: body.devices, token: req.session.token, ip: req.hostname});
                }
            }
        });
    });

    router.get('/products', function(req, res){
        request.get('http://localhost:3000/api/products', {
            headers:{
                Authorization: 'Bearer ' + req.session.token,
                Accept: 'application/json'
            }
        },
        (error, response, body) => {
            console.log(body);
            if(error || response.statusCode != 200){
                console.error(error);

                let err;

                if(response.statusCode == 401 || response.statusCode == 403)
                    err = 'Vous n\'avez pas les droits suffisants';
                else    
                    err = 'Une erreur est survenue';

                return res.render('products', {title: 'Produits', user: req.user, products: null, error: err, token: req.session.token, ip: req.hostname});
            }else{
                body = JSON.parse(body);

                if(body.error){
                    console.error(body.errorCode + ': ' + body.message);
                    return res.render('products', {title: 'Produits', user: req.user, products: null, error: 'Une erreur est survenue', token: req.session.token, ip: req.hostname});
                }else{
                    return res.render('products', {title: 'Produits', user: req.user, products: body.products, token: req.session.token, ip: req.hostname});
                }
            }
        });
    });

    return router;
}