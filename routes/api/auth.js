var express = require('express');
var db = require('../../models/index');
var authentication =  require('../../controllers/auth');

module.exports = function(app){
    var router = express.Router();

    router.post('/login', authentication.authenticate);

    return router;
}