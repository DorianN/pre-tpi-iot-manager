var express = require('express');
var db = require('../../models/index');
var productsController = require('../../controllers/products');
var authorize = require('../../middleware/authorize-api');

module.exports = function(app){
    var router = express.Router();

    //Get devices
    router.get('/', authorize(['get-products']), function(req, res){

        db.products.findAll({       
        }).then((products) => {
            res.status(200).json({
                error: false,
                products: products
            });
        }).catch((err) => {
            res.status(500).json({
                error: true,
                message: 'can\'t get products'
            });
        });
    });

    router.post('/', authorize(['add-products']), productsController.add);

    router.post('/delete', authorize(['delete-products']), productsController.delete);

    return router;
}