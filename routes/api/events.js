var express = require('express');
var db = require('../../models/index');
var eventController = require('../../controllers/users');
var authorize = require('../../middleware/authorize-api');

module.exports = function(app){
    var router = express.Router();

    router.get('/', authorize(['get-events']), function(req, res){
        db.events.findAll({       
        }).then((events) => {
            res.status(200).json({
                error: false,
                events: events
            });
        }).catch((err) => {
            res.status(500).json({
                error: true,
                message: 'can\'t get products'
            });
        });
        
    });

    return router;
}