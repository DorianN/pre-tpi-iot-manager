var express = require('express');
var db = require('../../models/index');
var usersController = require('../../controllers/users');
var authorize = require('../../middleware/authorize-api');

module.exports = function(app){
    var router = express.Router();

    router.get('/', authorize(['get-users']), function(req, res){
        var id = req.query.id;

        if(id){
            usersController.getFromId(id, (users) => {
                res.json(users);
            });
        }else{
            usersController.getAll((users) => {
                res.json(users);
            });
        }
    });

    router.post('/', authorize(['add-users']), function(req, res){
        var username = req.body.username;
        var email = req.body.email;
        var password = req.body.password;

        if(username && email && password){
            usersController.create(username, email, password, function(newUser, error){
                if(!error){
                    res.json(newUser);
                }else{
                    if(error.name == 'UniqueValueError'){
                        res.status(400).json({
                            error: true,
                            message: error.on + ' must be unique',
                            fields: error.on
                        });
                    }
                }
            });
        }else{
            res.status(400).json({
                error: true,
                message: 'Invalid data provided'
            });
        }

    });

    router.delete('/', authorize(['delete-users']), function(req, res){
        res.status(501).json({
            error: true,
            message: "Not implemented yet. WIP"
        });
    });

    return router;
}