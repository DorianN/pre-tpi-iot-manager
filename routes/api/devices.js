var express = require('express');
var db = require('../../models/index');
var devicesController = require('../../controllers/devices');
var authorize = require('../../middleware/authorize-api');

module.exports = function(app){
    var router = express.Router();

    //Get devices
    router.get('/', authorize(['get-devices']), function(req, res){

        db.devices.findAll({       
        }).then((devices) => {
            res.status(200).json({
                error: false,
                devices: devices
            });
        }).catch((err) => {
            res.status(500).json({
                error: true,
                message: 'can\'t get devices'
            });
        });
    });

    router.post('/', authorize(['add-devices']), devicesController.add);

    router.post('/authorize', authorize(['update-devices']), devicesController.authorize);

    //Data posted by devices
    router.post('/post', authorize(['post-data']), devicesController.postData);

    //Delete a device
    router.post('/unregister', function(req, res){
        res.status(501).json({
            error: true,
            message: "Not implemented yet. WIP"
        });
    });

    //Register a new device
    router.post('/authenticate', devicesController.authenticate);

    router.post('/delete', authorize(['delete-device']), devicesController.delete);

    return router;
}