'use strict';
module.exports = (sequelize, DataTypes) => {
  const devices_data = sequelize.define('devices_data', {
    deviceId: DataTypes.INTEGER,
    data: DataTypes.TEXT
  }, 
  {
    timestamps: true
  });
  devices_data.associate = function(models) {
    devices_data.belongsTo(models.devices);
  };
  return devices_data;
};