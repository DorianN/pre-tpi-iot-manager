'use strict';

module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('users', {
    username: {type: DataTypes.STRING, allowNull: false, unique: true},
    email: {type: DataTypes.STRING, allowNull: true, unique: true},
    password: {type: DataTypes.STRING},
    allowLogin: {type: DataTypes.BOOLEAN, allowNull: true}
  }, 
  {
    timestamps: true,
    defaultScope: {
      attributes: ['id', 'username', 'email', 'createdAt', 'updatedAt']
    }
  });

  Users.associate = function(models) {
    Users.belongsToMany(models.permissions, {through: 'users_permissions'});
  };

  //Hide protected data (password)
  Users.prototype.toJSON = function(){
    let attributes = Object.assign({}, this.get())

    delete attributes.password;
    return attributes;
  }

  return Users;
};