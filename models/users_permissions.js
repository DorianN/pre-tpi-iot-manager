'use strict';
module.exports = (sequelize, DataTypes) => {
  const users_permissions = sequelize.define('users_permissions', {
    userId: DataTypes.INTEGER,
    permissionId: DataTypes.INTEGER
  }, 
  {
    timestamps: true
  });
  users_permissions.associate = function(models) {
    users_permissions.belongsTo(models.permissions);
    users_permissions.belongsTo(models.users);
  };

  return users_permissions;
};