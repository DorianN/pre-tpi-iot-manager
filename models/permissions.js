'use strict';
module.exports = (sequelize, DataTypes) => {
  const permissions = sequelize.define('permissions', {
    name: DataTypes.STRING
  }, 
  {
    timestamps: true
  });
  permissions.associate = function(models) {
    permissions.belongsToMany(models.users, {through: 'users_permissions'});
  };

  return permissions;
};