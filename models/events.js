'use strict';
module.exports = (sequelize, DataTypes) => {
  const events = sequelize.define('events', {
    type: DataTypes.STRING,
    action: DataTypes.STRING,
    onDevice: DataTypes.INTEGER,
    onProduct: DataTypes.INTEGER,
    data: DataTypes.STRING
  }, 
  {
    timestamps: true
  });
  events.associate = function(models) {
    //events.belongsTo(models.products, {foreignKey: 'onProduct'});
    //events.belongsTo(models.devices, {foreignKey: 'onDevice'});
  };
  return events;
};