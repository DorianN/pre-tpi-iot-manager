'use strict';
module.exports = (sequelize, DataTypes) => {
  const products = sequelize.define('products', {
    name: DataTypes.STRING,
    price: DataTypes.FLOAT,
    stock_quantity: DataTypes.INTEGER
  }, 
  {
    timestamps: true
  });
  products.associate = function(models) {
    //products.hasMany(models.events, {foreignKey: 'onProduct'});
  };
  return products;
};