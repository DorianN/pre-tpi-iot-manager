'use strict';
module.exports = (sequelize, DataTypes) => {
  const devices = sequelize.define('devices', {
    device_name: DataTypes.STRING,
    active: DataTypes.BOOLEAN,
    last_active_datetime: DataTypes.DATE,
    authorized: DataTypes.BOOLEAN
  }, 
  {
    timestamps: true
  });
  devices.associate = function(models) {
    devices.hasMany(models.devices_data);
    //devices.hasMany(models.events, {foreignKey: 'onDevice'});
  };
  return devices;
};