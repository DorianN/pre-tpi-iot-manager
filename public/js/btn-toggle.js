jQuery.fn.btnToggle = function(){
    return this.each(function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
    
            if($(this).data('disabled'))
                $(this).val($(this).data('disabled'));
        }else{
            $(this).addClass('active');
    
            if($(this).data('enabled'))
                $(this).val($(this).data('enabled'));       
        }
    });
}