function apiCall(url, method, headers, data, callback){
    $.ajax({
        url: url,
        type: method,
        data: data,
        headers: headers,
        dataType: 'json',
        success: function(data){
            callback(false, data);
        },
        error: function(err){
            console.error(err.statusText);
            callback(err, null);
        }
    });
}