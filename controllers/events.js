var db = require('../models/index');
var productController = require('./products');

module.exports.deviceProductEvent = function(deviceId, data){
    var type = 'product';
    var action = data.action;
    var productId = data.productId;
    var quantity = data.quantity;

    if(action == 'take')
        productController.addStock(productId, -data.quantity);
    else if(action == 'place')
        productController.addStock(productId, data.quantity);

    db.events.create({
        type: type,
        action: action,
        onDevice: deviceId,
        onProduct: productId,
        data: quantity
    }).catch((err) => {
        if(err.parent.code == 'ER_NO_REFERENCED_ROW_2'){
            console.log('Error: DeviceProductEvent: unknow productId');
        }else{
            console.log(err);
        }
    });
}

module.exports.stockLimitEvent = function(product){
    db.events.create({
        type: 'stock_limit',
        action: '',
        onDevice: null,
        onProduct: product.id,
        data: 'Limite de stock atteinte ('+ product.stock_quantity +') pour le produit [' + product.name + ' (' + product.id + ')]'
    }).catch((err) => {
        if(err.parent.code == 'ER_NO_REFERENCED_ROW_2'){
            console.log('Error: DeviceProductEvent: unknow productId');
        }else{
            console.log(err);
        }
    });

    //send mail
}