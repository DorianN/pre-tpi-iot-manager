var db = require('../models/index');
var request = require('request');

module.exports.login = function(req, res){
    var username = req.body.username;
    var password = req.body.password;

    if(!username || !password){
        return res.redirect('/login?err=login&username=' + username);
    }

    request.post(
        'http://localhost:3000/api/auth/login',
        {json: {username: username, password: password}},
        function(error, response, body){
            if(!error){
                if(body.error){
                    if(body.errorCode == 'incorrect_login'){
                        return res.redirect('/login?err=login&username=' + username);
                    }else if(body.errorCode == 'loginNotAllowed'){
                        return res.redirect('/login?err=notAllowed&username=' + username);
                    }
                    else{
                        console.error(body.errorCode + ': ' + body.message);
                        return res.redirect('/login?err=&username=' + username);
                    }
                }else{
                    req.session.userId = body.user.id;
                    req.session.token = body.token;

                    req.session.save((error) => {
                        return res.redirect('/');
                    });
                }
            }else{
                console.error(error);
                return res.redirect('/login?err=api&username=' + username);
            }
        }
    );
}