var db = require('../models/index');
var passwordHash = require('password-hash');
const config = require('../config/app-config.json');
const jwt = require('jsonwebtoken');

module.exports.authenticate = function(req, res){
    var username = req.body.username;
    var password = req.body.password;

    if(!username || !password){
        return res.status(400).json({
            error: true,
            errorCode: 'incorrect_login',
            message: 'Incorrect credentials given'
        });
    }

    db.users.findAll({
        attributes: ['username', 'password', 'email', 'allowLogin'],
        where:{
            username: username
        },
        include:{
            model: db.permissions
        }
    }).then((user) => {
        if(user[0]){
            if(!user[0].allowLogin){
                return res.status(200).json({
                    error: true,
                    errorCode: 'loginNotAllowed',
                    message: 'Login is not allowed on this account'
                });
            }else{
                if(passwordHash.verify(password, user[0].password)){
                    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

                    const token = jwt.sign({
                        sub: user[0].id,
                        ip: ip
                        },
                        config.jwt.secret,{
                            expiresIn: 3600
                        }
                    );

                    return res.status(200).json({
                        error: false,
                        message: 'Login OK',
                        token: token,
                        user: user[0]
                    });
                }else{
                    return res.status(200).json({
                        error: true,
                        errorCode: 'incorrect_login',
                        message: 'Wrong username or password'
                    });        
                }
            }
        }else{
            return res.status(200).json({
                error: true,
                errorCode: 'incorrect_login',
                message: 'Wrong username or password'
            });              
        }     
    });
}