var db = require('../models/index');
var passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('../config/app-config.json');
var eventController = require('./events');

module.exports.postData = function(req ,res){
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    var deviceToken = req.header('Authorization');
    var payload = req.body;

    if(!deviceToken){
        return res.json({
            error: true,
            errorCode: 'InvalidToken',
            message: 'No token provided'
        });
    }else{
        deviceToken = deviceToken.substring(7);
        jwt.verify(deviceToken, config.jwt.secret, (err, decoded) => {
            if(err){
                return res.json({
                    error: true,
                    errorCode: 'InvalidToken',
                    message: 'erreur: ' + err
                });
            }else{
                if(decoded.ip != ip){
                    return res.json({
                        error: true,
                        errorCode: 'InvalidToken',
                        message: 'Invalid token'
                    });
                }else{
                    
                    db.devices.findAll({
                        where:{
                            id: decoded.deviceId
                        }
                    }).then((device) => {
                        if(device[0]){
                            if(!device[0].authorized){
                                return res.json({
                                    error: true,
                                    errorCode: 'notAuthorized',
                                    message: 'This device is not authorized'
                                });
                            }else{
                                db.devices_data.create({
                                    deviceId: decoded.deviceId,
                                    data: JSON.stringify(payload.data),
                                    createdAt: new Date(),
                                    updatedAt: new Date()
                                }).then((device_data) => {
                                    eventController.deviceProductEvent(decoded.deviceId, payload.data);

                                    return res.status(200).json({
                                        error: false,
                                        message: 'data posted'
                                    });
                                }).catch((err) => {
                                    console.log(err);
                        
                                    return res.status(500).json({
                                        error: true,
                                        errorCode: 'InternalError',
                                        message: 'Error occured with internal DB'
                                    });
                                });
                            }
                        }
                    });                    
                }
            }
        });  
    }
}

module.exports.add = function(req, res){
    var name = req.body.name;

    if(!name){
        return res.status(400).json({
            error: true,
            message: 'Missing data (name)'
        });
    }else{
        db.devices.create({
            device_name: name,
            active: false,
            last_active_datetime: null,
            authorized: false,
            createdAt: new Date(),
            updatedAt: new Date()
        }).then((device) => {
            return res.json({
                error: false,
                message: 'Device created'
            });
        });
    }
}

module.exports.authenticate = function(req, res){
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    var username = req.body.username;
    var password = req.body.password;
    var deviceToken = req.header('Authorization');

    if(!username || !password){
        return res.status(400).json({
            error: true,
            errorCode: 'incorrect_login',
            message: 'Incorrect credentials given'
        });
    }

    if(!deviceToken){
        db.users.findAll({
            attributes: ['username', 'password', 'email'],
            where:{
                username: username
            },
            include:{
                model: db.permissions
            }
        }).then((user) => {
            if(user[0]){
                if(passwordHash.verify(password, user[0].password)){
                    db.devices.create({
                        device_name: createid(20),
                        active: false,
                        last_active_datetime: new Date(),
                        authorized: false,
                        createdAt: new Date(),
                        updatedAt: new Date()
                    }).then((device) => {
                        const token = jwt.sign({
                            sub: user[0].id,
                            deviceId: device.id,
                            ip: ip
                            },
                            config.jwt.secret
                        );
    
                        return res.status(200).json({
                            error: false,
                            message: 'Authentication OK',
                            token: token,
                            device: device
                        });
                    });                 
                }else{
                    return res.status(200).json({
                        error: true,
                        errorCode: 'incorrect_login',
                        message: 'Wrong username or password'
                    });        
                }
                
            }else{
                return res.status(200).json({
                    error: true,
                    errorCode: 'incorrect_login',
                    message: 'Wrong username or password'
                });              
            }     
        });
    }else{
        deviceToken = deviceToken.substring(7);

        jwt.verify(deviceToken, config.jwt.secret, (err, decoded) => {
            if(err){
                return res.json({
                    error: true,
                    errorCode: 'InvalidToken',
                    message: 'Invalid token'
                });
            }else{
                console.log(decoded);

                if(decoded.ip == ip){
                    db.devices.findAll({
                        where: {
                            id: decoded.deviceId
                        }
                    }).then((device) => {
                        if(device[0]){
                            return res.json({
                                error: false,
                                message: 'Auth OK',
                                token: deviceToken,
                                device: device[0]
                            });
                        }else{
                            return res.json({
                                error: true,
                                errorCode: 'InvalidToken',
                                message: 'Invalid token'
                            });
                        }
                    });

                }else{
                    return res.json({
                        error: true,
                        errorCode: 'InvalidToken',
                        message: 'Invalid token'
                    });
                }
            }
        });
    }
}

module.exports.authorize = function(req, res){
    var deviceId = req.body.deviceId;
    var authorize = req.body.authorize;

    console.log(req.body);

    if(!deviceId || !authorize){
        return res.status(400).json({
            error: true,
            message: 'Invalid parameters'
        });
    }else{
        db.devices.findAll({
            where: {
                id: deviceId
            }
        }).then((devices) => {
            if(!devices[0]){
                return res.status(400).json({
                    error: true,
                    message: 'Invalid device id'
                });
            }else{
                devices[0].authorized = authorize;
                devices[0].save();

                return res.json({
                    error: false,
                    message: 'Device state updated'
                });
            }
        });
    }
}

module.exports.delete = function(req, res){
    var deviceId = req.body.deviceId;

    if(!deviceId){
        return res.status(400).json({
            error: true,
            message: 'Missing deviceId'
        });
    }

    db.devices.destroy({
        where:{
            id: deviceId
        }
    }).then((r) => {
        return res.json({
            error: false,
            message: 'Device deleted'
        });
    });

}

function createid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }