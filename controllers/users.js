var db = require('../models/index');
var passwordHash = require('password-hash');

module.exports.getAll = function(callback){
    db.users.findAll({
    }).then((users) => {
        callback(users);
    });
}

module.exports.getFromId = function(id, callback){
    db.users.findAll({
        where:{
            id: id
        }
    }).then((users) => {
        callback(users[0]);
    });
}

module.exports.create = function(username, email, password, callback){
    var hashedPassword = passwordHash.generate(password);

    db.users.create({
        username: username,
        email: email,
        password: hashedPassword
    }).then((newUser) => {
        callback(newUser);
    }).catch((e) => {
        if(e.name == 'SequelizeUniqueConstraintError'){
            callback(null, {name: 'UniqueValueError', on: Object.keys(e.fields)});
        }       
    });
}