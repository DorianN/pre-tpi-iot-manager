var db = require('../models/index');
var eventController = require('./events');

module.exports.addStock = function(productId, quantity){
    db.products.findAll({
        where:{
            id: productId
        }
    }).then((products) => {
        if(products[0]){
            products[0].stock_quantity += quantity;
            products[0].save();

            if(products[0].stock_quantity <= 5){
                eventController.stockLimitEvent(products[0]);
            }
        }
    });
}

module.exports.add = function(req, res){
    var name = req.body.name;
    var price = req.body.price;
    var quantity = req.body.quantity;

    if(!name || !price){
        return res.status(400).json({
            error: true,
            message: 'Wrong data'
        });
    }

    db.products.create({
        name: name,
        price: price,
        stock_quantity: quantity,
        createdAt: new Date(),
        updatedAt: new Date()
    }).then((product) => {
        return res.json({
            error: false,
            message: 'Product added'
        });
    });
}

module.exports.delete = function(req ,res){
    var productId = req.body.productId;

    if(!productId){
        return res.status(400).json({
            error: true,
            message: 'Missing productId'
        });
    }

    db.products.destroy({
        where:{
            id: productId
        }
    }).then((r) => {
        return res.json({
            error: false,
            message: 'Product deleted'
        });
    });
}