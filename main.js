const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const config = require('./config/app-config.json');
const db = require('./models/index');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const jwt = require('./middleware/jwt');
const errorHandlerAPI = require('./middleware/error-handler-api');
const path = require('path');
const passwordHash = require('password-hash');
var session = require('express-session')
const SequelizeStore = require('connect-session-sequelize')(session.Store);
const checkLogin = require('./middleware/check-login');

app.use(morgan('combined'));
app.use(session({
  secret: config.session.secret,
  resave: true,
  saveUninitialized: true,
  store: new SequelizeStore({
    db: db.sequelize
  }),
  cookie:{ maxAge: 1800000 }
}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '/public')));

//app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

db.sequelize.sync().then(() => {
    checkDefaultData();
    start();
});

function checkDefaultData(){
  db.users.findOrCreate({
    where:{
      username: 'admin'
    },
    defaults:{
      username: 'admin',
      email: 'admin',
      password: passwordHash.generate('Pa$$w0rd'),
      allowLogin: true
    }
  });

  db.users.findOrCreate({
    where:{
      username: 'device'
    },
    defaults:{
      username: 'device',
      email: 'device',
      password: passwordHash.generate('Pa$$w0rd'),
      allowLogin: false
    }
  });

  db.permissions.findOrCreate({
    where:{
      name: 'admin'
    },
    defaults:{
      name: 'admin'
    }
  });

  db.permissions.findOrCreate({
    where:{
      name: 'get-users'
    },
    defaults:{
      name: 'get-users'
    }
  });

  db.permissions.findOrCreate({
    where:{
      name: 'add-users'
    },
    defaults:{
      name: 'add-users'
    }
  });

  db.permissions.findOrCreate({
    where:{
      name: 'delete-users'
    },
    defaults:{
      name: 'delete-users'
    }
  });

  db.permissions.findOrCreate({
    where:{
      name: 'post-data'
    },
    defaults:{
      name: 'post-data'
    }
  });

  db.users_permissions.findOrCreate({
    where:{
      userId: 1,
      permissionId: 1
    },
    defaults:{
      userId: 1,
      permissionId: 1
    }
  });

  db.users_permissions.findOrCreate({
    where:{
      userId: 2,
      permissionId: 5
    },
    defaults:{
      userId: 2,
      permissionId: 5
    }
  });
}

function start(){
    app.listen(port);
    console.log('Started on port : ' + port);

    //Define routes
    app.use('/api/users', require('./routes/api/users')(app));
    app.use('/api/devices', require('./routes/api/devices')(app));
    app.use('/api/products', require('./routes/api/products')(app));
    app.use('/api/events', require('./routes/api/events')(app));
    app.use('/api/auth', require('./routes/api/auth')(app));
    app.use('/api', function(req, res){
      res.status(404).send();
    });

    //app.use('/api', errorHandlerAPI);

    app.use('/', checkLogin, require('./routes/website/web')(app));
}